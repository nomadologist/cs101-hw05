/**
 * 
 * @author Jack Booth
 * @version 11/8/16
 * Ball Class with size, xPos, yPos.  Can set and get positions and size and drop a ball.
 *
 */
public class Ball
{
//	Variable initiations
	private int size;
	private int xPos;
	private int yPos;
	
//	Constructors
	public Ball()
	{
		
	}
	
	public Ball(int size, int x, int y)
	{
		setSize(size);
		setPosition(x, y);
	}
	
/**
 * 	Sets size of the ball
 * @param size
 */
	public void setSize(int size)
	{
		this.size = size;
	}
	
/**
 * 	Sets the x and y positions of the ball
 * @param x - x position of the ball
 * @param y - y position of the ball
 */
	public void setPosition(int x, int y)
	{
		xPos = x;
		yPos = y;
	}
	
	/**
	 * 
	 * @return x position of the ball
	 */
	public int getXPosition()
	{
		return xPos;
	}
	
/**
 * 
 * @return size of the ball
 */
	public int getSize()
	{
		return size;
	}
	
/**
 * 
 * @return y position of the ball
 */
	public int getYPosition()
	{
		return yPos;
	}
	
/**
 * Drops a ball by one pixel (moves yPos up by 1).
 */
	public void drop()
	{
		yPos+=1;
	}
}
