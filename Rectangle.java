
public class Rectangle
{
	private int width;
	private int height;
	public static int count = 0;
	
	public Rectangle()
	{
		width = height = 1;
		count+=1;
	}
	
	public Rectangle(int width, int height)
	{
		this.width = width;
		this.height = height;
		count+=1;
	}
	
	public int getArea()
	{
		return width*height;
	}
	
	public int getPerimeter()
	{
		return (width*2) + (height*2);
	}
	
}
